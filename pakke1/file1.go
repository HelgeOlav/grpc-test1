package pakke1

import (
	p2 "bitbucket.org/HelgeOlav/grpc-test1/pakke2"
	"fmt"
)

func PrintSomething() {
	fmt.Println("pakke1 called")
	p2.PrintSomething()
}
