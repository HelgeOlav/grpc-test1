package main

import (
	"bitbucket.org/HelgeOlav/grpc-test1/messages"
	"context"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
)

// This file startes the gRPC server

var grpcServer = grpc.NewServer()

func startServer() {
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	lis, _ := net.Listen("tcp", ":8080")
	messages.RegisterMessage1Server(grpcServer, &server{})
	err := messages.RegisterMessage1HandlerFromEndpoint(context.Background(), mux, ":8080", opts)
	if err != nil {
		fmt.Println(err)
	} else {
		log.Println("Starting REST API")
		go http.ListenAndServe(":8081", mux)
	}
	err = grpcServer.Serve(lis)
	fmt.Println(err)
}
