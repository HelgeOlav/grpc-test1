package main

import (
	"bitbucket.org/HelgeOlav/grpc-test1/messages"
	"context"
	"testing"
)

func TestServer_AddUp(t *testing.T) {
	s := server{}
	// verify that we don't crash in nil input
	res, err := s.AddUp(context.Background(), nil)
	if res != nil || err == nil {
		t.Error("AddUp failed on nil input")
	}
	// verify right sum on input
	i := messages.NumberInput{Delay: 0, Number: []int32{3, 2, 1, 0, -1}}
	res, err = s.AddUp(context.Background(), &i)
	if err != nil && res != nil && res.Result != 5 {
		t.Error("AddUp does not sum correctly")
	}
	// check zero input numbers (and that it does not block on negative delays)
	i.Number = []int32{}
	i.Delay = 0 - 10
	res, err = s.AddUp(context.Background(), &i)
	if err != nil && res != nil && res.Result != 0 {
		t.Error("AddUp does not handle no input values")
	}
}
