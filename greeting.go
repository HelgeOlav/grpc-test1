package main

import (
	"bitbucket.org/HelgeOlav/grpc-test1/messages"
	"context"
	"fmt"
	"log"
	"sync"
	"time"
)

func showGreeting() {
	c := messages.NewMessage1Client(clientConn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()
	log.Print("testClient calles RPC")
	result, err := c.Greeting(ctx, &messages.NoInput{})
	if err != nil {
		log.Printf("showGreeting failed with %v\n", err)
	} else {
		fmt.Println(result)
	}
}

var greetingNumber int32 = 0
var greetingMux sync.Mutex

func (s *server) Greeting(ctx context.Context, in *messages.NoInput) (*messages.GreetingResult, error) {
	greetingMux.Lock()
	defer greetingMux.Unlock()
	msg := &messages.GreetingResult{
		Greeting: "Greetings from Earth",
		Number:   greetingNumber,
	}
	greetingNumber++
	return msg, nil
}
