package main

import (
	"bitbucket.org/HelgeOlav/grpc-test1/messages"
	"bitbucket.org/HelgeOlav/grpc-test1/pakke1"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"log"
	"time"
)

//go:generate protoc --go_out=plugins=grpc:messages rpc.proto

var clientConn *grpc.ClientConn

func testClient(delay int32) {
	log.Print("testClient started")
	nums := messages.NumberInput{
		Delay:  delay,
		Number: []int32{2, 4, 5, 12, 2, 5, -3, -4, 1231, 432, 34, -1500, -300, 24, 30, 1, 2, 3, 4, 5, 7, -1, -2, -1},
	}
	if clientConn == nil {
		var err error
		clientConn, err = grpc.Dial("localhost:8080",
			grpc.WithInsecure(), grpc.WithBlock(),
			grpc.WithPerRPCCredentials(BasicAuth{
				Username: "username",
				Password: "password",
			}),
			grpc.WithKeepaliveParams(keepalive.ClientParameters{
				Time:    60000,
				Timeout: 20,
			}))
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	c := messages.NewMessage1Client(clientConn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()
	log.Print("testClient calles RPC")
	result, err := c.AddUp(ctx, &nums)
	log.Print("testClient done calling RPC")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(result)
}

func main() {
	fmt.Println("gRPC-test1 started")
	pakke1.PrintSomething()
	go startServer()
	testClient(-2)
	go testClient(2)
	showGreeting()
	time.Sleep(24 * time.Hour)
	time.Sleep(5 * time.Second)
	log.Print("Ending application")
	if clientConn != nil {
		fmt.Println(clientConn.Close())
	}
}
