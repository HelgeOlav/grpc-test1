package main

import (
	"context"
	"encoding/base64"
	"google.golang.org/grpc/metadata"
	"log"
	"strings"
)

// support methods to handle gRPC authentication
// source: https://jbrandhorst.com/post/grpc-auth/

type BasicAuth struct {
	Username string
	Password string
}

// Client side methods

func (b BasicAuth) GetRequestMetadata(ctx context.Context, in ...string) (map[string]string, error) {
	log.Println("GetRequestMetadata() called")
	auth := b.Username + ":" + b.Password
	enc := base64.StdEncoding.EncodeToString([]byte(auth))
	return map[string]string{
		"authorization":    "Basic " + enc,
		"X-Greetings-From": "Helge Olav",
	}, nil
}

func (BasicAuth) RequireTransportSecurity() bool {
	return false
}

// Server side methods
// Returns true if user is authenticated
func ValidateUserFromContext(ctx context.Context) bool {
	log.Println("ValidateUserFromContext called")
	// get peer
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		log.Print("No metadata found")
		return false
	}
	// get auth header
	auth_header := md.Get("authorization")
	if len(auth_header) == 0 {
		log.Println("ValidateUserFromContext: no auth header found")
		return false
	}
	auth := auth_header[0]
	// work with auth
	const prefix = "Basic "
	if !strings.HasPrefix(auth, prefix) {
		log.Print("Missing Basic auth")
		return false
	}
	// check base64
	c, err := base64.StdEncoding.DecodeString(auth[len(prefix):])
	if err != nil {
		log.Print("Invalid base64")
		return false
	}
	// check syntax
	cs := string(c)
	s := strings.IndexByte(cs, ':')
	if s < 0 {
		log.Print("Invalid syntax in auth string")
	}
	// get password
	user, password := cs[:s], cs[s+1:]
	log.Printf("Found username %s - password %s\n", user, password)
	// check if right username
	if user != "username" {
		return false
	}
	return true
}
