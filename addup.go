package main

import (
	"bitbucket.org/HelgeOlav/grpc-test1/messages"
	"context"
	"errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

// this file implements the needful to implement the server part of the service

type server struct {
	messages.UnimplementedMessage1Server
}

func (s *server) AddUp(ctx context.Context, in *messages.NumberInput) (*messages.ResultInt, error) {
	// sanity check
	if in == nil {
		return nil, errors.New("No valid NumberInput")
	}
	// check user
	validated := ValidateUserFromContext(ctx)
	if !validated {
		return nil, status.Error(codes.Unauthenticated, "User not authenticated")
	}
	// check if we have to crash
	if in.Delay == messages.CrashProgram {
		//panic("Manual crash by AddUp")
		grpcServer.Stop()
	}
	// add up
	var result int32
	for _, num := range in.Number {
		result += num
	}
	// and pause
	if in.Delay > 0 {
		time.Sleep(time.Millisecond * time.Duration(in.Delay))
	}
	// return result
	return &messages.ResultInt{
		Result: result,
	}, nil
}
