# gRPC

This is my first attempt of using gRPC for messaging passing.

## Install and run

Install package using
```
go get -u google.golang.org/grpc
go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u bitbucket.org/HelgeOlav/grpc-test1
cd $GOHOME/src/bitbucket.org/HelgeOlav/grpc-test1
go generate
go build 
```

## Creating gRPC stubs

This can be a bit tricky from time to time. By adding $GOPATH/bin into your $PATH these commands worked on Centos 8.1.

You also need to enable the PowerTools repo in /etc/yum.repos and install
```
sudo dnf install protobuf-compiler protobuf-devel
```

### Creating gRPC stubs

```
protoc -I. -I/home/bruker/go/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  -I/usr/include \
  --go_out=plugins=grpc:messages \
  rpc.proto
```

### Creating REST API stubs

```
protoc -I. -I/home/bruker/go/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  -I/usr/include \
  --grpc-gateway_out=logtostderr=true:messages \
  rpc.proto
```

### Creating Swagger file

```
protoc -I. -I/home/bruker/go/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  -I/usr/include \
  --swagger_out=logtostderr=true:messages \
  rpc.proto
```
